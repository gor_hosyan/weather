git clone {repo}  
composer install  
cp .env.example .env   
php artisan key:generate  
update .env file DB connection values  
php artisan migrate  

php artisan serve or php -S localhost:8000 -t public  

-------

Basic UI with boostrap components. (without blade layout yield , just 1 file blade with all html)

Form Requests for Validations   
Main logic is in the Services   
CRUD functional is in the Repository     