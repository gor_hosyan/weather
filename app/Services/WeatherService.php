<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.12.2022
 * Time: 0:27
 */

namespace App\Services;

use App\Repositories\WeatherRepository;

class WeatherService
{
    /**
     * @var WeatherRepository
     */
    protected $weatherRepository;

    /**
     * WeatherService constructor.
     * @param WeatherRepository $weatherRepository
     */
    public function __construct(WeatherRepository $weatherRepository)
    {
        $this->weatherRepository = $weatherRepository;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function save($data)
    {
        $cityName = strtolower($data['city_name']);
        $updateOrCreateData = [
            'timestamp_dt'=> $data['timestamp'],
            'city_name'=> $cityName,
            'min_tmp'=> $data['min_tmp'],
            'max_tmp'=> $data['max_tmp'],
            'wind_speed'=> $data['wind_speed'],
        ];
        return $this->weatherRepository->saveOrUpdate($updateOrCreateData); 
    }

    /**
     * @param $city
     * @return mixed
     */
    public function fetch($city)
    {
        $city = strtolower($city);
        return $this->weatherRepository->fetch($city);
    }
    
}