<?php

namespace App\Services;
use Illuminate\Support\Facades\Http;

class ApiService
{
    /**
     * @param $city
     * @return mixed
     */
    public function fetch($city)
    {
        $response = Http::get('http://api.openweathermap.org/data/2.5/forecast?q='.$city.'&units=metric&appid=e4b8b08c185638b825af37facfe1fabb');
        $body = $this->reStructure($response->body());
        return $body;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function reStructure($data)
    {
        $data = json_decode($data);
        if($data->message === 0){
            if(empty($data->list)){
                $response['message'] = 'Empty data';
                $response['items'] = [];
                $response['success'] = false;
            }else{
                $city = $data->city->name;
                $createModelArray = function ($value) use ($city) {
                    $arr['timestamp']=$value->dt_txt;
                    $arr['city_name']=$city;
                    $arr['min_tmp']=$value->main->temp_min;
                    $arr['max_tmp']=$value->main->temp_max;
                    $arr['wind_speed']=$value->wind->speed;
                    return $arr;
                }; 

                $weathers = array_map($createModelArray, $data->list);
                $response['items'] = $weathers;
                $response['start'] = $data->list[0]->dt_txt;
                $response['end'] = $data->list[count($data->list)-1]->dt_txt;
                $response['message'] = 0;
                $response['success'] = true; 
            }
        }else{
            $response['message'] = $data->message;
            $response['items'] = [];
            $response['success'] = false;
        }
        return $response;
    }
     
}   