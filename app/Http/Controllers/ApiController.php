<?php

namespace App\Http\Controllers;
use App\Http\Requests\FetchRequest;
use App\Services\ApiService;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * @var ApiService
     */
    protected $apiService;

    /**
     * ApiController constructor.
     * @param ApiService $apiService
     */
    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @param FetchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetch(FetchRequest $request)
    {
        $call = $this->apiService->fetch($request->city);
        return response()->json($call);
    }
    
}
