<?php

namespace App\Http\Controllers;

use App\Http\Requests\FetchRequest;
use App\Http\Requests\StoreRequest;
use App\Services\WeatherService;

class ClientController extends Controller
{
    protected $weather;

    /**
     * ClientController constructor.
     * @param WeatherService $weatherService
     */
    public function __construct(WeatherService $weatherService)
    {
        $this->weather = $weatherService;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return view('client');
    }

    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(StoreRequest $request)
    {
        $this->weather->save($request->except('_token'));
        return response()->json([
            'message' => 'Data saved successfully',
            'success' => true,
        ]);
    }

    /**
     * @param FetchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetch(FetchRequest $request)
    {
        $data = $this->weather->fetch($request->city);
        return response()->json($data);
    }

}
