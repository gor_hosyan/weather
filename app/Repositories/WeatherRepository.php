<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.12.2022
 * Time: 0:27
 */

namespace App\Repositories;
use App\Models\Weather;


class WeatherRepository
{
    /**
     * @var Weather
     */
    protected $weather;

    /**
     * WeatherRepository constructor.
     * @param Weather $weather
     */
    public function __construct(Weather $weather)
    {
        $this->weather = $weather;
    }

    /**
     * @param $city
     * @return mixed
     */
    public function fetch($city)
    {
        return $this->weather->where('city_name', $city)->first();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function saveOrUpdate($data)
    {
        $save = $this->weather->updateOrCreate(
            ['city_name'=>$data['city_name']],
            $data
        );
        return $save;
    }
}