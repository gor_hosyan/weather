<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ClientController::class , 'index'])->name('index');
Route::post('/save', [ClientController::class , 'save'])->name('save');
Route::post('/fetch-from-api', [ApiController::class , 'fetch'])->name('fetch-from-api');
Route::post('/fetch-from-local', [ClientController::class , 'fetch'])->name('fetch-from-local');
