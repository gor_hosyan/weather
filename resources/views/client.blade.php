<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Weather</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <h1 class="text-center">Jacket or no Jacket</h1>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col"></div>
        <div class="col">
            <div class="mb-3">
                <input type="text" class="form-control" id="city" name="city" placeholder="Enter city name (E.g New York)">
            </div>
        </div>
        <div class="col">
            <button type="button" class="btn btn-primary" id="getFromApi">Get from api</button>
            <button type="button" class="btn btn-warning" id="getFromDb">Get from DB</button>
        </div>
        <div class="col"></div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <div id="apiResponseArea" class="border p-2 d-none">
                <h2 class="city_name"></h2>
                <h4>Period</h4>
                <div class="request_period"></div>
                <button class="btn btn-success" id="save">Save forecast</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div id="localResponseArea" class="d-none">
                <h2 class="city_name"></h2>
                <div class="updated_at"></div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col">
            <div id="responseTable" class="d-none">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Datetime</th>
                        <th>Min temp</th>
                        <th>Max temp</th>
                        <th>Wind speed</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
<script>
    $(function() {
        $(document).on('click','#getFromApi',function (e) {
            e.preventDefault();
            let city = $('#city').val();
            var firstElement;
            if(city){
                $('#responseTable table tbody').html('');
                $.ajax({
                    async: false,
                    method: "POST",
                    url: "/fetch-from-api",
                    data: { city: city , _token : '{{ csrf_token() }}' }
                }) .done(function( response ) {
                    if(response.message === 0){
                        let html = '';
                        response.items.forEach(function(item) {
                            html+= '<tr><td>'+item.timestamp+'</td><td>'+item.min_tmp+'</td><td>'+item.max_tmp+'</td><td>'+item.wind_speed+'</td></tr>'
                        });
                        $('#responseTable table tbody').html(html);
                        $('.city_name').text(city);
                        $('.request_period').html('<p>'+response.start+'</p><p>'+response.end+'</p>');
                        $('#localResponseArea').addClass('d-none');
                        $('#apiResponseArea').removeClass('d-none');
                        $('#responseTable').removeClass('d-none');

                        firstElement = response.items[0];
                    }else{
                        alert(response.message);
                    }
                });
                $(document).on('click','#save',function () {
                    $.ajax({
                        async: false,
                        method: "POST",
                        url: "/save",
                        data: {
                            timestamp:firstElement.timestamp ,
                            city_name:firstElement.city_name ,
                            min_tmp:firstElement.min_tmp ,
                            max_tmp:firstElement.max_tmp ,
                            wind_speed:firstElement.wind_speed ,
                            _token : '{{ csrf_token() }}'
                        }
                    }) .done(function( response ) {
                        alert(response.message);

                    });
                })
            }

        })

        $(document).on('click','#getFromDb',function (e) {
            e.preventDefault();
            let city = $('#city').val();

            if(city){
                $('#responseTable table tbody').html('');
                $.ajax({
                    async: false,
                    method: "POST",
                    url: "/fetch-from-local",
                    data: { city: city , _token : '{{ csrf_token() }}' }
                }) .done(function( response ) {
                    if(response.id){
                        let html= '<tr><td>'+response.timestamp_dt+'</td><td>'+response.min_tmp+'</td><td>'+response.max_tmp+'</td><td>'+response.wind_speed+'</td></tr>'
                        $('#responseTable table tbody').html(html);
                        $('.city_name').text(city);
                        $('.updated_at').html('<p>Updated at: '+response.updated_at+'</p>');
                        $('#apiResponseArea').addClass('d-none');
                        $('#localResponseArea').removeClass('d-none');
                        $('#responseTable').removeClass('d-none');
                    }else{
                        alert('not found');
                        setTimeout(function () {
                            window.location.reload();
                        })
                    }
                });
            }

        })
    });
</script>
</body>
</html>